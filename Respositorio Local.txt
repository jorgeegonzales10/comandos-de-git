# configuracion de usuario:git config
 - a nivel de sistema operativo: git config --system
 - a nivel global: se aplica a todos los repositorios de usuario: git config --global
 - a nivel local: se aplica a mi configuracion de repositorio actual:git config --local
-para ver configuracion actual:
 -git config --list --system:para nivel de sistema operativo
 -git config --list --global:para nivel global
 -git config --list --local:para nivel local
-para cambiar configuracion de usuario a nivel global:
 -git --global user.name "nombre de usuario"
 -git --global user.email "email@gmail.com"
-para cambiar configuracion de usuario a nivel local:
 -git --local user.name "nombre de usuario"
 -git --local user.email "email@gmail.com"

# inicializar un repositorio local:git init .
# añadir todos los archivos de la carpeta al area temporal: git add .
# añadir un unico fichero al area temporal: git add <nombre de fichero>
#confirmar ficheros: git commit -m "descripcion del commit"
#para añadir y confirmar: git commit -a 
 -Despues te pide la descripcion del commit
#para añadir y confirmar: 
 -git commit -a -m "descripcion del commit"
 -git commit -am "descripcion del commit"
#para listar los commit:git log
#para listar de manera resumida:git log --oneline
#para volver hacia atras en los commit:git reset
 -el Head usualmente es el ultimo commit.
 -git reset --hard:apunta el head hacia otro commit borrando archivos y cambios
   -git reset --hard HEAD~2(Numero de posiciones hacia atras)
   -git reset --hard apunta al ultimo estado conocido en tu repositorio local(.git)
 -git reset --soft:apunta el head hacia otro commit sin borrar archivos ni cambios
   -git reset --soft HEAD~3(Numero de posiciones hacia atras)
#para saber que commit a podido romper algo entre dos commit:git bisect
 -git bisect start: empieza la diseccion
 -git bisect good 398d0bfd(codigo de commit):ultimo commit bueno
 -git bisect bad 78a4f3fc(codigo de commit):commit el que dejo de funcionar
 -git bisect bad: indica que le commit actual es los commits malos
#para ver quien iso los cambios en el fichero:git blame fichero.txt(nombre de fichero)
#para ver los cambios que todavia no has enviado a tu repositorio local(.git):git diff
----RAMAS - BRANCH
#para ver la ramas actuales:git branch
#para crear rama(antes debes haber commiteado):
  - git branch <nombre de rama>
  - git checkout -b fix-12345(rama)(si la rama no existe ,la crea)
#para borrar una rama:
 -git branch -d <nombre-de-rama>
#para cambiar a otra rama:git checkout <nombre de rama>
#para listar commits en la rama actual:git log --oneline --graph
#para crear rama de un commit previo:
   - git branch <nombre de rama> 7127c60(codigo de commit desde donde se crea la rama)
#para crear una rama de otra rama:
  - git checkout -b <nombre de nueva rama> <rama de donde parte la nueva rama>
#para crear una rama en repositorio remoto(branches remotos):
 - git pull origin <nombre de la rama>
 - git checkout <nombre de la rama>
#para subir una rama creada en repositorio local al repositorio remoto:
 - git push -u origin <nombre de rama>
-----BRANCHING AND MERGIN
#Para fusionar ramas ubicarse en la rama que quedara luego de la fusion: 
  - git merge <nombre de la rama que se fusionara con la rama de la ubicacion actual>(esta desaparecera luego de la fusion)
  - tipos de merge
    -fast forward - no genera commit
    -recursiva - genera un commit con los cambios generados en la rma fusioanda
    -merge reescribiendo el historial de commit:
       .git rebase <nombre de rama a merge con rebase>
#para traer commits de una rama a otra:
  -ubicarse nal rama destino y aplicar el siguiente comando:
    - con los commits inicio y final incluidos:git cherry-pick 4e23fed^..22a6585
    - con los commits inicio y final sin incluir:git cherry-pick 4e23fed..22a6585
#para reservar cambios que no has terminado y que no puedes commitear:
  - git stash: para reservar cambios
  - git stash apply:para recuperar cambios
  - git stash list: para listar los stash
  - git stash clear: para borrar los stash
  - git stash save: para guardar un stash con un nombre concreto que prefieras.
  - git stash pop: patra abdicar los stash uno por uno.


#clientes de escritorio para procesar cambios(herramientas):
 -fork
 -tower
 -gitkraken
 -tortoise git
 -tortoise svn
 -tortoise cvn